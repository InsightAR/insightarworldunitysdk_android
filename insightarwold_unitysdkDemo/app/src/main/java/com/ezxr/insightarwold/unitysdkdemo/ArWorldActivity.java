package com.ezxr.insightarwold.unitysdkdemo;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ezxr.insight.comlibs.utils.MediaSaveUtil;
import com.ezxr.oasisunityplugin.utils.PermissionsUtil;
import com.netease.insightar.EzArInsight;
import com.netease.insightar.EzArWorldView;
import com.netease.insightar.ar.InsightARState;
import com.netease.insightar.ar.callback.OnArInsightUnityLoadingCallback;
import com.netease.insightar.ar.callback.RequestPermissionCallback;
import com.netease.insightar.callback.OnArInsightCaptureFileSavedCallback;
import com.netease.insightar.callback.OnArInsightDataCallback;
import com.netease.insightar.callback.OnArInsightNetworkDataObtainCallback;
import com.netease.insightar.callback.OnArInsightRecordCallback;
import com.netease.insightar.callback.OnArInsightResultListener;
import com.netease.insightar.entity.EzArAlgResult;
import com.netease.insightar.entity.EzArLandmark;
import com.netease.insightar.entity.EzArLandmarkQueryParam;
import com.netease.insightar.entity.EzArRenderResult;
import com.netease.insightar.entity.message.IAr3dEventMessage;
import com.netease.insightar.entity.message.Screenshot3dEventMessage;
import com.netease.insightar.exception.ArResourceNotFoundException;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class ArWorldActivity extends AppCompatActivity
    implements OnArInsightResultListener, OnArInsightCaptureFileSavedCallback,
    OnArInsightRecordCallback, OnArInsightUnityLoadingCallback, RequestPermissionCallback {

    private static final int REQUEST_CODE_PERMISSION = 101;
    private final int REQUEST_CODE_LOCATION = 111;
    private final int REQUEST_CODE_BLUETOOTH = 112;
    private static final String LANDMARK_CONTENT_ID = "11";

    private EzArWorldView mArWorldView;
    private View mLoadingLayout;
    private TextView mLoadingTV;
    private View mMaskLoadingLayout;
    private TextView mMaskLoadingTv;
    private ImageView mMaskLoadingIV;
    private Animation mRotateAnimation;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar_world);

        mArWorldView = findViewById(R.id.ezArWorldView);
        mLoadingTV = findViewById(R.id.resourceLoadingTV);
        mLoadingLayout = findViewById(R.id.loadingLayout);
        mMaskLoadingIV = findViewById(R.id.maskLoadingIV);
        mMaskLoadingTv = findViewById(R.id.maskLoadingTV);
        mMaskLoadingLayout = findViewById(R.id.maskLoadingLayout);
        // create EzArWorldView
        mArWorldView.create();
        mArWorldView.setRequestPermissionCallback(this);
        LinearInterpolator interpolator = new LinearInterpolator();
        mRotateAnimation = AnimationUtils.loadAnimation(this, R.anim.loading_circle_rotate);
        mRotateAnimation.setInterpolator(interpolator);

        boolean isCameraGranted = isPermissionGranted(this, Manifest.permission.CAMERA);
        boolean isWriteGranted =
            isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean isAudioRecodeGranted = isPermissionGranted(this, Manifest.permission.RECORD_AUDIO);

        if (isCameraGranted && isWriteGranted && isAudioRecodeGranted) {
            loadData();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
            }, REQUEST_CODE_PERMISSION);
        }
        findViewById(R.id.takePhoto).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mArWorldView.doArTakePhoto();
            }
        });
        //开始录屏按钮
        findViewById(R.id.startRecord).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mArWorldView.doArStartRecord();
            }
        });
        //结束录屏按钮(录屏时，如果当前acitivity执行了pause操作，则录屏会自动停止，并回调finish结果出来
        findViewById(R.id.stopRecord).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mArWorldView.doArStopRecord();
            }
        });
    }

    private void loadData() {
        // TODO: LANDMARK_CONTENT_ID有外部传入，或通过以下方法获取：
        // EzArInsight.getLandmarkListAll();——获取和该APP_KEY相关联的所有Landmark数据列表
        // EzArInsight.getLandmarksNearby();——获取特定经纬度范围内的Landmark数据列表
        EzArLandmarkQueryParam queryParam =
            new EzArLandmarkQueryParam.Builder().setCoordinateType(2)   // 坐标类型
                .setLatitude(30.244181552367408)  // 纬度，Demo中写死，接入时一般传入用户当前具体的经纬度坐标
                .setLongitude(120.22718731893394) // 经度，Demo中写死，接入时一般传入用户当前具体的经纬度坐标
                .setMaxDistance(4000 * 10000)  // 距离上述经纬度最大范围，单位：米
                .setTagIds("")  // Tag ID，预留字段，默认传空字符串
                .build();
        EzArInsight.getLandmarkListAll(queryParam,
            new OnArInsightNetworkDataObtainCallback<List<EzArLandmark>>() {
                @Override
                public void onNetworkDataSucc(@Nullable List<EzArLandmark> ezArLandmarks) {
                    if (null == ezArLandmarks || ezArLandmarks.isEmpty()) {
                        Toast.makeText(ArWorldActivity.this, "该地理围栏内没有可体验AR内容", Toast.LENGTH_SHORT)
                            .show();
                        return;
                    }

                    // 根据返回列表加载其中一个Landmark内容，Demo里为简便写死为第一个，具体接入时按需求即可（比如先将ezArLandmarks数据作为列表展示出来）
                    EzArInsight.loadLandmarkDataByCid(ezArLandmarks.get(0).getLandmarkId(),
                        new SceneResourceFetchCallback(ArWorldActivity.this));
                }

                @Override public void onNetworkDataError(int i, String s) {
                    Toast.makeText(ArWorldActivity.this, "数据获取错误：" + i + "-" + s,
                        Toast.LENGTH_SHORT).show();
                }
            });
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean isPermissionGranted = true;
            String per;
            for (int i = 0; i < permissions.length; i++) {
                per = permissions[i];
                if (per.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) || per.equals(
                    Manifest.permission.CAMERA) || per.equals(Manifest.permission.RECORD_AUDIO)) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isPermissionGranted = false;
                        break;
                    }
                }
            }
            if (!isPermissionGranted) {
                Toast.makeText(this, "你已禁止权限...", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            loadData();
        } else if (requestCode == 1) {
            for (int i = 0; i < permissions.length; i++) {
                if (!isPermissionGranted(this, permissions[i])
                    && !ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permissions[i])) {//拒绝不再询问
                    mArWorldView.setPermissionResult(permissions[i], -2);
                } else if (grantResults.length > 0) {
                    mArWorldView.setPermissionResult(permissions[i], grantResults[i]);
                }
            }
        }
    }

    private void showAr(String id, String destFilePath) {
        if (TextUtils.isEmpty(destFilePath) || !new File(destFilePath).exists()) {
            showErrorView("本地资源包异常...");
            return;
        }
        try {
            mArWorldView.doArShowView(id, destFilePath);
            showLoadingView("场景资源加载中...", 0);
        } catch (ArResourceNotFoundException e) {
            showErrorView("资源包异常...");
            e.printStackTrace();
        }
    }

    private void showLoadingView(String loadingStr, int loadType) {
        if (loadType == 1) {
            if (mMaskLoadingLayout.getVisibility() != View.VISIBLE) {
                mMaskLoadingLayout.setVisibility(View.VISIBLE);
                mMaskLoadingIV.startAnimation(mRotateAnimation);
            }
            mMaskLoadingTv.setText(loadingStr);
        } else {
            if (mLoadingLayout.getVisibility() == View.GONE) {
                mLoadingLayout.setVisibility(View.VISIBLE);
            }
            mLoadingTV.setText(loadingStr);
        }
    }

    protected void hideLoadingView(int loadType) {
        if (loadType == 1) {
            mMaskLoadingIV.clearAnimation();
            mMaskLoadingLayout.setVisibility(View.GONE);
        } else {
            mLoadingLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override protected void onStart() {
        super.onStart();
        if (null != mArWorldView) {
            // EzArWorldView start
            mArWorldView.start();
            mArWorldView.registerArInsightResultListener(this);
            //注册拍照监听
            mArWorldView.registerArInsightCaptureListener(this);
            mArWorldView.registerArRecordListener(this);
            mArWorldView.registerArInsightUnityLoadingListener(this);
        }
    }

    @Override protected void onResume() {
        super.onResume();
        if (null != mArWorldView) {
            // EzArWorldView resume
            mArWorldView.resume();
        }
    }

    @Override protected void onPause() {
        super.onPause();
        if (null != mArWorldView) {
            // EzArWorldView pause
            mArWorldView.pause();
        }
    }

    @Override protected void onStop() {
        super.onStop();
        if (null != mArWorldView) {
            // EzArWorldView stop
            mArWorldView.stop();
            mArWorldView.unregisterArInsightResultListener(this);
            mArWorldView.unRegisterArInsightCaptureListener(this);
            mArWorldView.unRegisterArRecordListener(this);
            mArWorldView.unRegisterArInsightUnityLoadingListener(this);
        }
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        if (null != mArWorldView) {
            // EzArWorldView destroy
            mArWorldView.destroy();
        }
    }

    @Override public void on3dEventMessage(IAr3dEventMessage iAr3dEventMessage) {
        switch (iAr3dEventMessage.type()) {
            case IAr3dEventMessage.TYPE_CLOSE_AR:
                finish();
                break;
            case IAr3dEventMessage.TYPE_OPEN_URL:
                break;
            case IAr3dEventMessage.TYPE_RELOAD_AR:
                break;
            case IAr3dEventMessage.TYPE_SCREEN_RECORD_START:
                break;
            case IAr3dEventMessage.TYPE_SCREEN_RECORD_STOP:
                break;
            case IAr3dEventMessage.TYPE_SHARE:
                break;
            case IAr3dEventMessage.TYPE_SCREENSHOT:
                Screenshot3dEventMessage aa = (Screenshot3dEventMessage) iAr3dEventMessage;
                System.out.println("iAr3dEventMessage:" + aa.getImagePath());
                break;
            default:
                break;
        }
    }

    @Override public void onArEngineResult(EzArAlgResult ezArAlgResult) {

    }

    @Override public void onCaptureFinish(String s) {
        showErrorView("截屏成功:" + s);
        //保存到相册.可以调用下面的代码，也可以改成自己的实现
        MediaSaveUtil.saveMediaFile(this, s, 0);
    }

    @Override public void onCaptureError(String s) {
        showErrorView("截屏失败:" + s);
    }

    @Override public void onRecordStart() {
        showErrorView("开始录屏");
    }

    @Override public void onRecordFinish(String s, @Nullable Bitmap bitmap) {
        showErrorView("录屏成功:" + s);
        //保存到相册.可以调用下面的代码，也可以改成自己的实现
        MediaSaveUtil.saveMediaFile(this, s, MediaSaveUtil.MEDIA_TYPE_VIDEO);
    }

    @Override public void onRecordError(String s) {
        showErrorView("录屏失败:" + s);
    }

    @Override public void onUnityLoading(int loadType, int downloadProgress) {
        if (loadType == 0) {//主场景资源加载，要覆盖全屏
            showLoadingView("资源加载中" + downloadProgress + "%", loadType);
        } else {//蒙层布局，一般用于子场景资源加载，如果没有该场景，可以忽略
            showLoadingView("内容加载中" + downloadProgress + "%", loadType);
        }
    }

    @Override public void onUnityLoadingComplete(int loadType) {
        hideLoadingView(loadType);
        mArWorldView.showMap();
    }

    @Override public void onArRenderResult(EzArRenderResult ezArRenderResult) {
        System.out.println("ezArRenderResult:" + ezArRenderResult.getArState());
        switch (ezArRenderResult.getArState()) {
            case InsightARState.INIT_OK:
                break;
            case InsightARState.INIT_FAIL:
                showErrorView("unity加载ar场景资源错误，请退出重试");
                break;
        }
    }

    private boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
            == PackageManager.PERMISSION_GRANTED;
    }

    @Override public void requestPermissionAccess(String permission) {
        PermissionsUtil.with(this).addPermission(permission).initPermission();
    }

    @Override public void requestOpenGpsSwitch() {//如果当前内容不需要gps定位，该方法可以不实现
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    @Override public void requestBluetoothSwitch() {//如果当前内容不需要蓝牙定位，该方法可以不实现
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intent, REQUEST_CODE_BLUETOOTH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LOCATION) {
            mArWorldView.setGpSwitchResult();
        } else if (requestCode == REQUEST_CODE_BLUETOOTH) {
            mArWorldView.setBluetoothSwitchResult();
        }
    }

    private static class SceneResourceFetchCallback
        implements OnArInsightDataCallback<EzArLandmark> {

        private final WeakReference<ArWorldActivity> mActivityRef;

        SceneResourceFetchCallback(ArWorldActivity activity) {
            mActivityRef = new WeakReference<>(activity);
        }

        @Override public void onDownloadStart(String id) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            // 显示开始下载交互UI
            ins.showLoadingView("资源下载中...", 0);
        }

        @Override public void onDownloadProgress(String id, int progress) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            // 显示下载进度交互UI
            ins.showLoadingView("资源下载中..." + progress + "%", 0);
        }

        @Override public void onDownloadError(String id, int code, String msg) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            ins.hideLoadingView(0);
            // 显示下载失败交互UI
            ins.showErrorView("Error: " + code + ":" + msg);
        }

        @Override public void onDownloadPause(String id) {

        }

        @Override public void onDownloadResume(String id) {

        }

        @Override public void onDownloadSucc(String id) {

        }

        @Override public void onNetworkDataSucc(@Nullable EzArLandmark ezArLandmark) {

        }

        @Override public void onNetworkDataError(int code, String msg) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            ins.hideLoadingView(0);
            // 显示数据获取错误交互UI
            ins.showErrorView("Error: " + code + ":" + msg);
        }

        @Override public void onUnzipStart(String id) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            // 显示正在解压交互UI
            ins.showLoadingView("开始解压", 0);
        }

        @Override public void onUnzipEnd(String id, String destFilePath) {
            ArWorldActivity ins = mActivityRef.get();
            if (null == ins) {
                return;
            }
            // 解压完成加载AR场景资源
            ins.showAr(id, destFilePath);
        }
    }
}
