package com.ezxr.insightarwold.unitysdkdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.insightar.EzArInsight;
import com.netease.insightar.callback.OnArInsightDeviceSupportCallback;
import com.netease.insightar.entity.EzArSupportStatus;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView sdkVersionNo = findViewById(R.id.sdkVersionNo);
        sdkVersionNo.setText("SDK版本号：" + EzArInsight.getSDKVersionNo());

        findViewById(R.id.enterArWorld).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                checkArSupport(new Runnable() {
                    @Override public void run() {
                        startActivity(new Intent(MainActivity.this, ArWorldActivity.class));
                    }
                });
            }
        });
    }

    private void checkArSupport(Runnable runnable) {
        EzArInsight.isArSupportOnline(this, new OnArInsightDeviceSupportCallback() {
            @Override public void onIsDeviceArSupport(EzArSupportStatus ezArSupportStatus) {
                if (!ezArSupportStatus.isArSupport()) {
                    Toast.makeText(MainActivity.this, "当前设备暂不支持Ar体验，请切换设备尝试", Toast.LENGTH_SHORT)
                        .show();
                } else if (!ezArSupportStatus.isArCalibrated()) {
                    Toast.makeText(MainActivity.this, "暂未支持此设备，正在努力扩展中", Toast.LENGTH_SHORT).show();
                } else {
                    //支持ar体验
                    runnable.run();
                }
            }

            @Override public void onNetworkDataError(int i, String s) {
                Toast.makeText(MainActivity.this, "无网络，请检查网络链接", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
