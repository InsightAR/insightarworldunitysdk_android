### InsightAR-World UnitySDK  
  
#### Release Note  

**v1.9.7**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 优化定位权限请求条件，主场景和POI模式下不请求权限；

**v1.9.6**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 优化华为系手机ar体验，由于activity生命周期变化导致ar画面卡顿问题；
- 替换语音识别sdk方案(之前用的已经不可用了)；

**v1.9.5**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 优化云定位平滑应用策略；
- 修复2001场景下，trackingstate没有单独输出状态bug；

**v1.9.4**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增unity支持Simple Waypoint System（路径动画）、Spine Animator（脊椎链动画）

**注：使用导航功能至少需要用1.6.3版本编辑器**

**v1.9.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 修复部分华为设备体验AR时弹出安装引导异常


**v1.9.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增动态更新导航UI功能；
- 更新导航3D UI；
- 修复导航功能用户距离下一个拐点距离比终点距离远的bug；
- 修复相机被遮挡时，出现导航箭头置灰并置于地图原点的bug；
- 修复IOS端解析Geo文件异常；
- 优化贴地箭头显示逻辑；
- 优化跨楼层导航交互逻辑；
- 优化半屏地图下拉按钮交互；
- 优化初次云定位不稳定的问题；
- 优化SDK标定数据获取逻辑；
- 优化SDK黑名单校验逻辑；

注：使用导航功能至少需要用1.6.3版本编辑器


**v1.8.2**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增动态更新导航UI功能；
- 新增内容层输出DUBUG插件；
- 修复导航功能相关BUG；


**v1.8.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 修复1.7.0版本导致的相机渲染不同步的问题；


**v1.8.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增融合定位功能算法模块，融合数据包括GPS、蓝牙、视觉云定位等，支持定位结果的无感切换；
- 修复开启实时阴影后摄像头纹理渲染出现拖影bug；
- 优化权限申请逻辑，根据内容对应的地图需要进行权限申请；
- 优化云定位模块，提升云定位结果正确率；


**v1.7.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 修复导航算法在不可通行区域定位成功后一直显示重新定位bug


**v1.7.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增拍照完成事件回调，并将图片路径返回内容层
- 新增录制完成事件回调，并将视频路径返回内容层
- 优化AR实景分辨率，运行AR-World跟踪算法的屏上分辨率提升到设备屏幕分辨率


**v1.6.2**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增支持unityAB包动态加载功能
- 新增unity的project组件支持、视频全屏播放功能
- 优化SDK子事件更新逻辑
- 优化AR追踪算法，提高暗光环境下算法鲁棒性
- 修复华为手机相机bug
- 修复导航回调时间冲突bug


**v1.6.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增主动定位功能
- 新增云定位平滑优化策略
- 新增自动开启端上引擎HDR与阴影优化效果
- 修复相关已知bug

**v1.6.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 云定位策略优化
- 地图功能优化
- 适配高通SM4350/SM6225/MT6779V等硬件平台

**v1.5.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 添加语音识别接口
- 增加权限申请接口

**v1.4.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 添加水印功能
- 修复google arcore及华为arengine算法运行错误导致应用crash问题

**v1.4.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 导航解耦:支持主场景，poi，2d地图，导航四种模式
- 算法兼容google arcore1.27.0，华为arengine3.5.0.1版本

**v1.3.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 增加ARcore，AREngine安装引导
- 资源下载增加重试机制，最多尝试3次

**v1.2.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/ad916e78-d8f2-49d4-a309-5b8df99ea9c0)    
> [FAQ](https://near.yuque.com/docs/share/5ab0129b-5fa5-4dd4-8614-4de0ee836897)

- 修复云定位使用的图像内参更换为VIO运行时估算的图像内参
- 修复ezmap打包问题导致android系统低于10时sdk启动会crash问题

**v1.2.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 新增unity资源加载进度的回调接口
- 支持录屏功能
- 内容层新增接口，获取当前跟踪算法类型(arcore,华为arengine，自研算法等)

**v1.1.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

- 支持子场景资源包下载
- 支持内容层截屏，分享调用
- 支持内容层发送网络请求，下载文件
- 云定位状态优化
- 优化算法，提高稳定性

**v1.0.0**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/insightarworldunitysdk_android/tags)下载）

> [接入参考文档](https://yixian-ezxr.feishu.cn/wiki/wikcnmtLRHPcQfS89cI2HswpZYd)
> [FAQ](https://yixian-ezxr.feishu.cn/wiki/wikcnFNUfI5GdrxGrTJOOPuDVjd)

**基础功能**

- 支持视觉定位（云定位、端云协同定位）
- 支持内容热更新（JS实现AR内容脚本逻辑）
- 提供AR基础生命周期的支持
